<?php
/**
 * BiP Extension: Exhibitors Post Type
 * Description: Create Exhibitors post type and load ACF
 * @author: Chris Thompson
 * @url: https://bitbucket.org/thompc/extension-exhibitors-cpt
 * @version: 1.1
 */

//Register our post type
add_action( 'init', function() {
    $args = array(
        "labels" => array("name" => "Exhibitors", "singular_name" => "Exhibitor"),
        "description" => "",
        "public" => true,
        "show_ui" => true,
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "exhibitor", "with_front" => true ),
        "query_var" => true,
        'supports' => array('title', 'editor')
    );
    register_post_type( "exhibitor", $args );
});

//Get ACF to search our /acf path for our .JSON file
add_filter('acf/settings/load_json', 'load_exhibitors_acf');
function load_exhibitors_acf( $paths ) {
    $paths[] = dirname(__FILE__).'/acf';
    return $paths;
}

// Image size
add_image_size( 'exhibitor_logo', 200, 200, false);
